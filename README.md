# balenalib base image smoke test

Testing a subset of the [balenalib base images][balenalib docs] on actual hardware,
using [GitLab runners on balenaOS devices][gitlab runner project].

Tests include:

* basic operating system information
* package update and install
* language stack information
* compile/run of a hello-world project

... and hopefully more in the future. See `test.sh` and the files linked from there for the actual tests

### Usage

* modifying tests are done in the relevant script files
* modifying the images tested are done by updating `config.yml` and regenerating the relevant `.gitlab-ci.yml` with running
  `make`. This might need some libraries to be available for Python, those can be installed from `requirements.txt`, for example:

```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

[balenalib docs]: https://www.balena.io/docs/reference/base-images/base-images/ "Balena base images documentation"
[gitlab runner project]: https://github.com/balena-io-playground/balena-gitlab-runner "Gitlab Runner as a balena application"